/** @format */

import {AppRegistry} from 'react-native';
import PushNotificationScreen from './PushNotificationScreen';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => PushNotificationScreen);
