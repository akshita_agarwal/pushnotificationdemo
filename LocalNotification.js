import React,{Component} from 'react';
import {
 Text,
 View,
 Button,
} from 'react-native';
import NativeEventEmitter from 'NativeEventEmitter'
import { PushNotificationIOS } from 'react-native';
var PushNotification = require('react-native-push-notification');
export default class LocalNotification extends Component{



  componentDidMount() {
          PushNotification.configure({
        
            // (optional) Called when Token is generated (iOS and Android)
            onRegister: function(token) {
                console.log( 'TOKEN:', token );
            },
        
            // (required) Called when a remote or local notification is opened or received
            onNotification: function(notification) {
                console.log( 'NOTIFICATION:', notification );
                 notification.finish(PushNotificationIOS.FetchResult.NoData);
            },
        
         
            senderID: "YOUR GCM (OR FCM) SENDER ID",
        
           
            permissions: {
                alert: true,
                badge: true,
                sound: true
            },
        
            
            popInitialNotification: true,
        
          
            requestPermissions: true,
        });
       };

      //  const localNotification = () => {
      //   PushNotification.localNotification({
      //     autoCancel: true,
      //     largeIcon: "ic_launcher",
      //     smallIcon: "ic_notification",
      //     bigText: "My big text that will be shown when notification is expanded",
      //     subText: "This is a subText",
      //     color: "green",
      //     vibrate: true,
      //     vibration: 300,
      //     title: "Notification Title",
      //     message: "Notification Message",
      //     playSound: true,
      //     soundName: 'default',
      //     actions: '["Accept", "Reject"]',
      //   });
      //  };
 

  handleOnPress2 = ()=>{

    PushNotification.localNotificationSchedule({
      //... You can use all the options from localNotifications
      message: "My Notification Message", // (required)
      date: new Date(Date.now() + (6 * 1000)) // in 60 secs
    });
 }

    render(){
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
       
            <Text>
            Press the button to see push Notification
            </Text>
            <Button
            title={'Press Me'}
            onPress={this.handleOnPress2}/>
    </View>
    
    )}

 
}
