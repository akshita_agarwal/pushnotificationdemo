/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import stripe from 'tipsi-stripe'
import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


export default class App extends Component {
  componentDidMount(){
  
    stripe.setOptions({
      publishableKey: 'pk_test_4K7PmjfmEs7PX3YLWdGv69hx',
      merchantId: 'merchant.com.dotsquares.ApplePayDemo', // Optional
     
    })
    stripe.openNativePaySetup()
    const items = [{
      label: 'Whisky',
      amount: '50.00',
    }, {
      label: 'Tipsi, Inc',
      amount: '50.00',
    }]
    
    const shippingMethods = [{
      id: 'fedex',
      label: 'FedEX',
      detail: 'Test @ 10',
      amount: '10.00',
    }]
    
    // const options = {
    //   requiredBillingAddressFields: ['all'],
    //   requiredShippingAddressFields: ['phone', 'postal_address'],
    //   shippingMethods,
    // }
    
  stripe.paymentRequestWithApplePay(items).then((res)=>{
   alert(res)
  })
  }
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome t     ytytrytr6o React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
